import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RestService} from "./service/rest.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ListComponent} from "./components/list/list.component";
import { CreateComponent } from './components/create/create.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    RestService,
    HttpClient,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
