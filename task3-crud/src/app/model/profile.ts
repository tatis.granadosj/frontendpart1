export interface Profile {
  id:number;
  firstName: string;
  lastName:string;
  age: number;
  gender: string;
}
