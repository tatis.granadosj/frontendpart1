import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Profile} from "../model/profile";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http:HttpClient) { }

  get(): Observable<Profile[]> {
    return this.http.get<Profile[]>('http://localhost:8081/profiles');
  }

  delete(id:number):Observable<boolean> {
    return this.http.delete<boolean>("http://localhost:8081/profiles/"+id);
  }

  add(profile:Profile): Observable<Profile> {
    return this.http.post<Profile>('http://localhost:8081/profiles', profile);
  }

  update(profile:Profile):Observable<Profile> {
    return this.http.put<Profile>('http://localhost:8081/profiles',profile);
  }

}
