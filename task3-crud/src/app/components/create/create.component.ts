import { Component, OnInit } from '@angular/core';
import {Profile} from "../../model/profile";
import {RestService} from "../../service/rest.service";
import {NgForm} from "@angular/forms";
import {take} from "rxjs/operators";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  newProfile: Profile;

  constructor(private rest:RestService) { }

  ngOnInit(): void {
    this.initObject();
  }

  initObject():void {
    // this.newProfile = new Profile();
  }

  submit(childForm:NgForm):void {
    this.rest.add(this.newProfile).pipe(take(1)).subscribe((profile:Profile)=>{
      if (profile) {
        alert("ha sido creado");
        childForm.reset();
      }
    });
  }

}
