import {Component, OnInit, OnDestroy} from '@angular/core';
import {Profile} from "../../model/profile";
import {BehaviorSubject, Subject, Subscription} from "rxjs";
import {RestService} from "../../service/rest.service";
import {take, takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  profiles: Profile[];
  destroy: Subject<boolean>;

  constructor(private dataRest: RestService) {
    this.destroy = new Subject();
  }

  ngOnInit(): void {
    this.get();
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
  }

  simulateDestroy():void {
    this.destroy.next(true);
  }

  get(): void {
    this.dataRest.get().pipe(takeUntil(this.destroy)).subscribe((response: Profile[])=>{
      if(response){
        this.profiles = response;
      }
    });
  }

  delete(profile: Profile): void {
    const res = confirm("Quiere eliminar a "+profile.firstName+" "+profile.lastName+" ?");
    if (res) {
      this.dataRest.delete(profile.id).pipe(takeUntil(this.destroy)).subscribe((response:boolean)=>{
        alert(+profile.firstName+" "+profile.lastName+"ha sido eliminado");
        this.get();
      });
    }
  }

  edit(profile:Profile):void{
    this.dataRest.update(profile).subscribe((profile:Profile)=>{
      if (profile){
        //profile.reset();
      }
    })
  }
}
