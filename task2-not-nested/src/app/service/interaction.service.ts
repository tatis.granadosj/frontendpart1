import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {Profile} from "../model/profile";

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private currentProfile: BehaviorSubject<Profile>;

  constructor() {
    this.currentProfile = new BehaviorSubject<Profile>(null);
  }

  getCurrentProfile$(): Observable<Profile> {
    return this.currentProfile.asObservable();
  }

  setCurrentProfile$(profile:Profile):void {
    this.currentProfile.next(profile);
  }

}
