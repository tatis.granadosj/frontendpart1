export interface Profile {
  firstName: string;
  lastName:string;
  age: number;
  gender: string;
}
