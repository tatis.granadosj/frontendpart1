import {Component, OnDestroy, OnInit} from '@angular/core';
import {Profile} from "../../model/profile";
import {InteractionService} from "../../service/interaction.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy {

  profile: Profile;
  profileSubscription: Subscription;

  // public detail: Profile;
  // private _interactionService: InteractionService;
  // private personList: Profile[] = []; //persons

  constructor(private interactionService: InteractionService) {
    this.profileSubscription = new Subscription();
  }

  ngOnInit():void {
    this.getSelectedData();
  }

  ngOnDestroy(): void {
    this.profileSubscription.unsubscribe();
  }

  getSelectedData():void {
    this.interactionService.getCurrentProfile$().subscribe( (profile: Profile) => {
      if (null !== profile){
        this.profile = profile;
      }
    });
  }
}
