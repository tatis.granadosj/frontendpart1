import {Component, OnInit} from '@angular/core';
import {InteractionService} from "../../service/interaction.service";
import {Profile} from "../../model/profile";

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {

  myMultipleProfiles: Profile[];
  selected: Profile;

  constructor(private interactionService: InteractionService) {
  }

  ngOnInit(): void {
    this.initData();
  }

  setSelected(profile): void {
    this.interactionService.setCurrentProfile$(profile);
  }

  clickProfile (profile:Profile){
    this.interactionService.setCurrentProfile$(profile);
  }

  initData(): void {
    this.myMultipleProfiles = [
      {
        firstName: "John",
        lastName: "Doe",
        age: 30,
        gender: "Male"
      },
      {
        firstName: "Peter",
        lastName: "Fernandez",
        age: 25,
        gender: "Male"
      },
      {
        firstName: "Andrea",
        lastName: "Fernandez",
        age: 27,
        gender: "Male"
      },
      {
        firstName: "Lois",
        lastName: "Vans",
        age: 20,
        gender: "Male"
      }
    ];
  }
}
