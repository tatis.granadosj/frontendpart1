import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ItemMenuComponent } from './components/item-menu/item-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemMenuComponent
  ],
    imports: [
        BrowserModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule{ }
