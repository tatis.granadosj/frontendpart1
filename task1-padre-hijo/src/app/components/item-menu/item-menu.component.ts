import {Component, Input, OnInit} from '@angular/core';
import {MenuItem} from "../../model/menu-item";

@Component({
  selector: 'app-item-menu',
  templateUrl: './item-menu.component.html',
  styleUrls: ['./item-menu.component.css']
})
export class ItemMenuComponent implements OnInit {

  @Input() menuItems: MenuItem[];

  constructor() {
    this.menuItems = [];
  }

  ngOnInit(): void {
  }

  // onClickItem(item: Menu): void{
  // }
}
