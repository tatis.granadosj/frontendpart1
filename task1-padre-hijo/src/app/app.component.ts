import { Component } from '@angular/core';
import {MenuItem} from "./model/menu-item";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task1-padre-hijo';
  menuItems: MenuItem [] =[];
  menuItems2: MenuItem [] =[];

  constructor() {
    this.menuItems = [
      {
        path:'#',
        label:'Universidad Técnica',
        icon: 'se-user',
        active: false
      },
      {
        path:'news',
        label:'News',
        icon: 'se-news1',
        active: false
      },
      {
        path:'directory',
        label:'Directory',
        icon: 'se-directory',
        active: false
      },
      {
        path:'worked',
        label:'Worked',
        icon: 'se-worker',
        active: false
      },
      {
        path:'#',
        label:'Pave Portal',
        icon: 'se-portal',
        active: false
      }
    ];

    this.menuItems2 = [
      {
        path:'mail',
        label:'Mail',
        icon: 'se-mail',
        active: false
      },
      {
        path:'chat',
        label:'Chat',
        icon: 'se-comment',
        active: false
      },
      {
        path:'schedule',
        label:'Schedule',
        icon: 'se-calendar',
        active: false
      }
    ];
  }
}
