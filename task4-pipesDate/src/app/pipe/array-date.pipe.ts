import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'arrayDate'
})
export class ArrayDatePipe  implements PipeTransform {
  transform(value: string){
    let now = new Date();
    let valueDate = new Date(value);
    let rest = Math.round(Math.abs((now.getTime() - valueDate.getTime())/1000));
   // return rest/(1000*60*60*24);

    let result: string;
    const sec = 60;
    const min = sec*60;
    const hour = min*24;
    const day = hour*7;
    const sem = day*7;
    if (rest < sec) {
      result = 'hace ' + Math.floor(rest) + ' Segundos';
    }
    else if (rest < min) {
      result = 'hace ' + Math.floor(rest / sec) + ' Minutos';
    }
    else if (rest < hour) {
      result = 'hace ' + Math.floor(rest / min) + ' Horas';
    }
    else if (rest < day) {
      result = 'hace ' + Math.floor(rest / hour) + ' Dias';
    }
    else if (rest < sem) {
      result = 'hace ' + Math.floor(rest / day) + ' Semanas';
    }
    else {
      result = 'El ' + moment(valueDate).format('DD/MM/YYYY') ;
    }
    return result;
  }
}
