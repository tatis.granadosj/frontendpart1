import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task4-pipesDate';
  fechas =['09/02/2021','08/27/2021','08/26/2021', '08/01/2021','03/30/1981'];
}
